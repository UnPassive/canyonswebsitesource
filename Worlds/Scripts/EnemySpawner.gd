extends Timer

var spawn_rate_lower_limit = .05
var spawn_rate_upper_limit = 1.5	#todo: consider adding max zombie count to not excede
var min_spawn_distance = 300	#todo: make sure isn't pixel based (harder on high-res screens)
var max_spawn_distance = 1000
var enemy_preload = preload("res://Enemies/Zombie-Smasher/Zombie-Smasher.tscn")

func _ready():
	start(1)
	

func _on_Timer_timeout():
	var player = get_parent().get_node("Player")
	var position = player.position
	var x_distance = rand_range(min_spawn_distance, max_spawn_distance)
	var y_distance = rand_range(min_spawn_distance, max_spawn_distance)

	var direction = floor(rand_range(0, 2))
	if(direction == 0):
		x_distance = x_distance * -1
	direction = floor(rand_range(0, 2))
	if(direction == 0):
		y_distance = y_distance * -1

	position.x += x_distance
	position.y += y_distance
	
	var enemy = enemy_preload.instance()
	enemy.position = position
	enemy.set_target(player)	#todo: consider having enemy wait for a bit before moving
	get_parent().add_child(enemy)
	print("timer signal")
	start(rand_range(spawn_rate_lower_limit, spawn_rate_upper_limit))
	#todo: spawn z and assign target?
