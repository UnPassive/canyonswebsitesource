extends KinematicBody2D

const MOVE_SPEED = 300
var health = 200
var damage = 100
const type = "Player"
onready var raycast = $RayCast2D


#func _ready():
	#yield(get_tree(), "idle_frame")	#wait a frame
	#get_tree().call_group("enemies", "set_target", self)	#Set target for all objects in group "enemies"

#todo: make player walk around for ammo


func _physics_process(delta):
	var move_vector = Vector2()

	if Input.is_action_pressed("move_up"):
		move_vector.y -= 1
	if Input.is_action_pressed("move_down"):
		move_vector.y += 1
	if Input.is_action_pressed("move_left"):
		move_vector.x -= 1
	if Input.is_action_pressed("move_right"):
		move_vector.x += 1

	move_vector = move_vector.normalized()
	move_and_collide(move_vector * MOVE_SPEED * delta)
	
	var look_vector = get_global_mouse_position() - global_position
	global_rotation = atan2(look_vector.y, look_vector.x)
	
	if Input.is_action_just_pressed("shoot"):
		var collision = raycast.get_collider()
		if raycast.is_colliding() and collision.has_method("take_damage"):
			collision.take_damage(damage)

func get_type():
	return type

func take_damage(damage_received):
	health -= damage_received
	if health <= 0:
		get_tree().reload_current_scene()
		#todo: implement menu screen, show score and have restart button
