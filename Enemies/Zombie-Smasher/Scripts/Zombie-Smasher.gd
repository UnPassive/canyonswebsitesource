extends KinematicBody2D

const MOVE_SPEED = 200
var health = 100
var damage = 100
var type = "Enemy"
var target = null
onready var raycast = $RayCast2D

func _ready():
	add_to_group("enemies")
	
func _physics_process(delta):
	if target == null:
		return
	
	var vector_to_target = target.global_position - global_position
	vector_to_target = vector_to_target.normalized()
	global_rotation = atan2(vector_to_target.y, vector_to_target.x)
	move_and_collide(vector_to_target * MOVE_SPEED * delta)
	
	if raycast.is_colliding():
		var collision = raycast.get_collider()
		if collision.has_method("get_type") and collision.get_type() == "Player" and collision.has_method("take_damage"):
			collision.take_damage(damage)

func take_damage(damage_received):
	health -= damage_received
	if health <= 0:
		queue_free()

func set_target(new_target):
	target = new_target

func get_type():
	return type
